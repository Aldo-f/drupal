<?php

namespace Drupal\new_route\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase
{

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection)
  {
    // Change path '/user/login' to '/login'.
    if ($route = $collection->get('user.login')) {
      $route->setPath('/login');
    }
    // Always deny access to '/user/logout'.
    // Note that the second parameter of setRequirement() is a string.
    if ($route = $collection->get('user.logout')) {
      $route->setPath('/logout');
      $route->setRequirement('_access', 'TRUE');
    }

    if ($route = $collection->get('is_front')) {
      $route->setPath('/test');
    }

    // // Define custom access for '/user/login'.
    // if ($route = $collection->get('user.login')) {
    //   $route->setRequirement('_custom_access', 'Drupal\new_route\Access\StandardAccessCheck::access');
    // }
    // // Define custom access for '/user/logout'.
    // if ($route = $collection->get('user.logout')) {
    //   $route->setRequirement('_custom_access', 'new_route.services_access_checker::access');
    // }
  }
}