<?php

namespace Drupal\hello_calibrate\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "hello_calibrate_example",
 *   admin_label = @Translation("Example"),
 *   category = @Translation("Hello Calibrate")
 * )
 */
class ExampleBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build['content'] = [
      '#markup' => $this->t(
        "Hello Calibrate block
        <ul class='menu menu--tools nav'>
          <li class='first'>
            <a href='/hello-calibrate/example' data-drupal-link-system-path='hello-calibrate/example'>Controller</a>
          </li>
        </ul>"
      ),
    ];
    return $build;
  }
}