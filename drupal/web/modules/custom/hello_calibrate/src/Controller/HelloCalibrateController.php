<?php

namespace Drupal\hello_calibrate\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Hello Calibrate routes.
 */
class HelloCalibrateController extends ControllerBase
{


  /**
   * Builds the response.
   */
  public function build()
  {

    $person = (object)[
      name => 'Aldo',
      email => 'info@aldofieuw.com',
      friends => ['pol', 'anne']
    ];
    $sum  = 5 + 20;
    $time  = strval(date("d/m/Y G:i:s", time()));

    // dump($sum);
    // dump($person);
    // dump($time);

    // print($sum);
    // print_r($person);
    // print($time);

    $build['content'] = [
      '#type' => 'item',
      // '#markup' => $this->t("Hello Calibrate<br>The current time is:"),
      '#markup' => $this->t("Hello Calibrate<br>The current time is: " . $time),
    ];

    return $build;
  }
}