/**
 * @file
 * Calibrate behaviors.
 */


(function ($, Drupal) {

  'use strict';

  /**
   * Behavior description.
   */
  Drupal.behaviors.calibrate = {
    attach: function (context, settings) {
      $(function () {
        // get correct height from top to nav
        $('.view-display-id-block_1').waitForImages().done(function () {
          let stickyOffset = $('.sticky').offset().top;
          scrollEvent(stickyOffset);
        });

        // scrollevent
        function scrollEvent(stickyOffset) {
          $(window).scroll(function () {
            let sticky = $('.sticky'),
              scroll = $(window).scrollTop();

            if (scroll >= stickyOffset)
              sticky.addClass('fixed');
            else
              sticky.removeClass('fixed');
          });
        }
      });
    }
  };
}(jQuery, Drupal));
