<?php

/**
 * @file
 * Theme settings form for Calibrate theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function calibrate_form_system_theme_settings_alter(&$form, &$form_state)
{

  $form['calibrate'] = [
    '#type' => 'details',
    '#title' => t('Calibrate'),
    '#open' => TRUE,
  ];

  $form['calibrate']['font_size'] = [
    '#type' => 'number',
    '#title' => t('Font size'),
    '#min' => 12,
    '#max' => 18,
    '#default_value' => theme_get_setting('font_size'),
  ];
}