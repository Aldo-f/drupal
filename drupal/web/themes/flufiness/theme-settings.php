<?php

/**
 * @file
 * Theme settings form for Flufiness theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function flufiness_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['flufiness'] = [
    '#type' => 'details',
    '#title' => t('Flufiness'),
    '#open' => TRUE,
  ];

  $form['flufiness']['font_size'] = [
    '#type' => 'number',
    '#title' => t('Font size'),
    '#min' => 12,
    '#max' => 18,
    '#default_value' => theme_get_setting('font_size'),
  ];

}
