# Project voor solicitatie
Local develop van een drupal 8 site 

* [x] Content type News voorzien van Paragraphs (https://www.drupal.org/project/paragraphs)
* [x] Custom controller ‘Hello World’ pagina (niet voorzien in wireframes) (https://www.drupal.org/docs/8/api/routing-system/introductory-drupal-8-routes-and-controllers-example)
* [x] Custom Display Suite veld (niet voorzien in wireframes) (https://bitbucket.org/kevinhowbrook/d8-custom-ds-fields)
* [x] Custom Block in code (niet voorzien in wireframes) (https://www.drupal.org/docs/8/creating-custom-modules/create-a-custom-block)
* [x] Een extra zoekveld op pagina ‘Articles’ (niet voorzien in wireframes) die zal zoeken doorheen alle teksten van de articles.
* [x] Het zoekveld moet ook een autocomplete functionaliteit bezitten (gebruik maken van search api en facets).
* [x] Drupal opzetten en modules installeren met Composer (eventueel manueel met een zip werkt ook, maar voorkeur gaat uit naar Composer – zoals bij Laravel/Symfony)
* [x] Pas de access callback van een route aan d.m.v. een RouteSubscriber (niet voorzien in wireframes) (https://www.drupal.org/docs/8/api/routing-system/altering-existing-routes-and-adding-new-routes-based-on-dynamic-ones)


## Content types
* [x] ``News`` (body, image, tags)
* [x] ``Article`` (body, comments, image, tags)
* [x] ``Office`` (body, image, tags (country), tel, fax, adres, contact person, email)

## Functioneel
* [x] ``Articles``
  * [x] kunnen gekoppeld worden aan een office (niet verplicht)
    * [x] daarboven zijn ze (automatisch) gekoppeld aan dat land
  * [x] hebben (eventueel) tags uit een taxonomy lijst.
  * [x] er kan gefilterd worden in de taxonomy-lijst
  * [x] facet search
  * [x] zoekveld met autocomplete
  * [x] zoeken in alle fields van Articles
* [x] ``Offices`` 
  * [x] hebben een country-veld waarop gefilterd kan worden (options)
* [x] ``News`` 
  * [x] eventueel ook bij zoeken integreren 
  * [x] voorzien van [Paragraphs](https://www.drupal.org/project/paragraphs) 
* [x] Custom controller ([Hello World](https://www.drupal.org/docs/8/api/routing-system/introductory-drupal-8-routes-and-controllers-example))
* [ ] [Custom Display Suite veld ](https://bitbucket.org/kevinhowbrook/d8-custom-ds-fields/src/master/)
* [x] Access callback van een route aanpassen d.m.v. een [RouteSubscriber](https://www.drupal.org/docs/8/api/routing-system/altering-existing-routes-and-adding-new-routes-based-on-dynamic-ones) 

## Visueel   
* [x] Custom theme 
  * [x] Basis responsive 
  * [x] m.b.h.v. SASS
    * [x] menu
      * [x] versmalt als je naar boven scrolt 
      * [x] blijft fixed staan (na het scrollen)

  
-------
## Extra's
### Functioneel
* [ ] ``Basis page `` (body)
### Visueel
* [ ] Bootstrap
  * [ ] Sub theme
* [ ] Semantic UI
  * [ ] Sub theme

```
vagrant up
vagrant ssh

drush -v
drush list
drush help pm-list 

# Generate content
#drush devel-generate-content
drush genc 
drush genc --types=news 20 --skip-fields image

# Download and enable theme 
composer require 'drupal/bootstrap:^3.19'
drush theme:enable bootstrap
drush then bootstrap

# Download and enable module (composer & drush)
composer require 'drupal/paragraphs:^1.8' && drush en paragraphs -y
drush en paragraphs -y

# Remove module (does not remove module from composer.json)
#drush pmu 
#drush pm-uninstall paragraphs 

# Generate module/theme/routing/..
drush gen
drush gen module-standard
drush gen theme
drush gen theme-setting
drush gen yml-routing

# Clear all cache
drush cache-rebuild 
drush cr

# Import and export DB
drush sql-dump > dump.sql
drush sql-drop
drush sql-cli < dump.sql

# Import & Export Site Configuration
drush config-export --destination=config/site
drush config-import --source=config/site
```

### Te gebruiken modules
``` 
# Composer
composer require 'drupal/address:^1.7' 
composer require 'drupal/bootstrap:^3.19' 
composer require 'drupal/paragraphs:^1.8' 
composer require 'drupal/ds:^3.3' 
composer require 'drupal/facets:^1.4' 
composer require 'drupal/admin_toolbar:^1.27'
composer require 'drupal/adminimal_theme:^1.5'
composer require 'drupal/pathauto:^1.4'
composer require 'drupal/better_exposed_filters:^3.0'  
composer require 'drupal/search_api:^1.13'
composer require 'drupal/search_api_autocomplete:^1.2'
#composer require 'drupal/search_api_solr:^3.1' 
composer require 'drupal/easy_breadcrumb:^1.12'
composer require 'drupal/paragraphs_trimmed:^1.0'   #fix for bodt of Paragraph in Header Image
composer require 'drupal/scss_compiler:^1.0'        #fix for not abe to use Ruby (


# Enable
drush en address -y
drush then bootstrap -y 
drush en paragraphs -y
drush en ds -y 
drush en facets -y 
drush en devel devel_generate -y
drush en admin_toolbar -y
drush en adminimal_theme -y
drush en pathauto -y 
drush en better_exposed_filters -y
drush en search_api search_api_autocomplete -y
#drush en search_api_solr -y
drush en easy_breadcrumb -y
drush en media_library -y
drush en search_api_db -y
```

```
# install sass-compiler
sudo apt install ruby-full
sudo apt install ruby-compass
gem install sass 
compass watch --poll
```



----------------------------------

### E.v.t. nog te doen
* Apache Solr Search gebruiken naast database Search
  * https://www.ostraining.com/blog/drupal/apache-solr/
  * https://www.drupal.org/docs/8/modules/search-api-solr
  * https://www.drupal.org/project/search_api_solr
  * https://www.drupal.org/node/1999280

---------


```
vagrant_hostname: drupal.test
vagrant_machine_name: drupal
vagrant_ip: 192.168.99.99

vagrant_synced_folders:
  # The first synced folder will be used for the default Drupal installation, if
  # any of the build_* settings are 'true'. By default the folder is set to
  # the drupal-vm folder.
  - local_path: .
    destination: /var/www/drupal
    type: nfs
    create: true

apache_vhosts:
  - servername: "{{ drupal_domain }}"
    serveralias: "www.{{ drupal_domain }}"
    documentroot: "{{ drupal_core_path }}"
    extra_parameters: "{{ apache_vhost_php_fpm_parameters }}"

  - servername: "calibrate.dev"  
    serveralias: "www.calibrate.dev"
    documentroot:  "/var/www/drupalvm/drupal-calibrate/web" 
    extra_parameters: "{{ apache_vhost_php_fpm_parameters }}"

mysql_users:
  - name: "{{ drupal_db_user }}"
    host: "%"
    password: "{{ drupal_db_password }}"
    priv: "*.*:ALL"
```

```
composer create-project drupal-composer/drupal-project:8.x-dev some-dir --no-interaction
```


--------

### Sources: 
* [drush](http://docs.drush.org/en/master/)
* [drupalVM](http://docs.drupalvm.com/en/latest/)  
* [drushCommands](https://drushcommands.com/drush-9x/)
* [Database Search](https://medium.com/@swappyp20/creating-a-faceted-search-view-in-drupal-8-using-the-search-api-modules-2b2604ad37f4)
* [Paragraphs](https://www.ostraining.com/blog/drupal/paragraphs-module/)
* [RouteSubscriber](https://www.drupal.org/docs/8/api/routing-system/altering-existing-routes-and-adding-new-routes-based-on-dynamic-ones) 
* [Custom Display Suite veld ](https://bitbucket.org/kevinhowbrook/d8-custom-ds-fields/src/master/)
* [custom controller](https://www.drupal.org/docs/8/api/routing-system/introductory-drupal-8-routes-and-controllers-example)
* [Search API](https://www.drupal.org/docs/8/modules/search-api)
* [Search API Autocomplete](https://www.drupal.org/docs/8/modules/search-api-autocomplete)
* [facets](https://drupal8.support/en/modules/facets) 
* [theming](https://www.drupal.org/docs/8/theming)
* [sticky menu after scroll](https://codepen.io/AldoF/pen/qzxvbE)